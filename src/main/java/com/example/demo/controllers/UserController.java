package com.example.demo.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.User;
import com.example.demo.services.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/create")
	public String create(@RequestBody User user) {
		userService.create(user);
		return user.toString();
	}
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> getUsers(){
		return new ResponseEntity<List<User>>(userService.getAll(),HttpStatus.OK);
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUser(@PathVariable String id){
		return new ResponseEntity<User>(userService.getUser(id),HttpStatus.OK);
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable String id){
		if(userService.updateUser(id, user)) {
			return new ResponseEntity<User>(user,HttpStatus.CREATED);
		}else {
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		
	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable String id){
		if(userService.delete(id)) {
			return new ResponseEntity<String>("Usuario "+ id +" eliminado",HttpStatus.OK);
		}else {
			return new ResponseEntity<String>("Usuario "+ id +" no existe",HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
}
