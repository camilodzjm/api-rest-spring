package com.example.demo.models;
import com.example.demo.models.Car;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")
@Document(collection = "users")
public class User implements Serializable{
	
	@Id 
	private String _id;
	private String firstName;
	private String lastName;
	private String email;
	private List<Car> cars;
	
	public User(String firstName, String lastName, String email, List<Car> cars) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.cars = cars;
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Car> getCars() {
		return cars;
	}
	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", cars=" + cars + "]";
	}


	
}
