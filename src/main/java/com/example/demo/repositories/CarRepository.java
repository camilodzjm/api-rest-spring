package com.example.demo.repositories;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.Car;

@Repository
public interface CarRepository extends MongoRepository<Car, Serializable>{
	
}
