package com.example.demo.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.User;

@Repository
public interface UserRepository extends MongoRepository<User, Serializable>{
	public User findByFirstName(String firstName);
	public User findByLastName(String lastName); 
	
}
