package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	//create Operation
	public User create(User nuevoUsuario) {
		return userRepository.save(nuevoUsuario);
	}
	
	public List<User> getAll(){
		return userRepository.findAll();
	}
	
	public User getUser(String id) {
		if(userRepository.findById(id).isPresent()) {
			return userRepository.findById(id).get();	
		}else {
			return null;
		}
	}
	
	public User getByFirstName(String firstName) {
		return userRepository.findByFirstName(firstName);
	}
	
	public boolean updateUser(String id, User user) {
		Optional<User> nUser = userRepository.findById(id);
		if(nUser.isPresent()) {
			nUser.get().setEmail(user.getEmail());
			nUser.get().setFirstName(user.getFirstName());
			nUser.get().setLastName(user.getLastName());
			nUser.get().setCars(user.getCars());
			userRepository.save(nUser.get());
			return true;
		}else {
			return false;
		}
		
		
	}
	
	public void deleteAll() {
		userRepository.deleteAll();
	}
	
	public boolean delete(String id) {
		try {
			userRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}	
	}
	
	
}	
